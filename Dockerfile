FROM registry.gitlab.com/ibero-fisicamoderna/environments/archlinux-env:master

# INSTALL BASE PACKAGES
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm make cmake xerces-c qt5-base glu openmotif soqt

RUN useradd -m builder

# CLONE AND BUILD
USER builder

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4.git
WORKDIR /home/builder/geant4
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-abladata.git
WORKDIR /home/builder/geant4-abladata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-ensdfstatedata.git
WORKDIR /home/builder/geant4-ensdfstatedata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-incldata.git
WORKDIR /home/builder/geant4-incldata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-ledata.git
WORKDIR /home/builder/geant4-ledata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-levelgammadata.git
WORKDIR /home/builder/geant4-levelgammadata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-neutronhpdata.git
WORKDIR /home/builder/geant4-neutronhpdata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-particlexsdata.git
WORKDIR /home/builder/geant4-particlexsdata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-piidata.git
WORKDIR /home/builder/geant4-piidata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-radioactivedata.git
WORKDIR /home/builder/geant4-radioactivedata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-realsurfacedata.git
WORKDIR /home/builder/geant4-realsurfacedata
RUN makepkg

WORKDIR /home/builder
RUN git clone https://aur.archlinux.org/geant4-saiddata.git
WORKDIR /home/builder/geant4-saiddata
RUN makepkg

# INSTALL AND DELETE BUILDS
USER root

WORKDIR /home/builder
RUN pacman -U --noconfirm `ls geant4/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-abladata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-ensdfstatedata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-incldata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-ledata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-levelgammadata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-neutronhpdata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-particlexsdata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-piidata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-radioactivedata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-realsurfacedata/*.pkg.tar`
RUN pacman -U --noconfirm `ls geant4-saiddata/*.pkg.tar`

# SETUP WORKDIR
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# CLEANUP
RUN userdel builder && rm -rf /home/builder
RUN sudo pacman -Scc --noconfirm && sudo pacman -Rns --noconfirm $(pacman -Qtdq) || echo 'Nothing to remove'

# ENTRYPOINT
COPY entrypoint /bin
ENTRYPOINT ["/bin/entrypoint"]

